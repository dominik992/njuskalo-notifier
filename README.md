# Install instructions
- Install node.js and npm
- `npm i`

# Project setup
- create .env file
- add the following content in it:
```
EMAIL_SENDER=yourEmail
EMAIL_SENDER_PASSWORD=yourmailPassword
RECEIVER_EMAIL=yourEmail
```
- adjust src/config.js file. It contains `filters` object.
Fill it with your filters with the following format:
```
{
    yourFilterName: njuskaloFilterPath,
    yourFilterName2: njuskaloPath2,
}
```

# Run
- run the script with `node src`