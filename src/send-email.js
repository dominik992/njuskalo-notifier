import nodemailer from 'nodemailer';
import Promise from 'bluebird';
import config from './config';

export default function sendMail(text) {
  const senderEmail = config.senderEmail.replace('@', '%40');
  const transportUrl = `smtps://${senderEmail}:${config.senderPassword}@smtp.gmail.com`;
  const transporter = nodemailer.createTransport(transportUrl);
  const mailOptions = {
    from: `"Njuskalo notifier" <${config.senderEmail}>`,
    to: config.receiverEmail,
    subject: 'Nova njuškalo nekretnina!',
    html: text,
  };
  Promise.promisifyAll(transporter);
  return transporter.sendMailAsync(mailOptions);
}
