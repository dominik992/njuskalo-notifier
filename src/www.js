import 'dotenv/config';
import _ from 'lodash';
import fs from 'fs';
import path from 'path';
import config from './config';
import * as njuskalo from './njuskalo';
import sendMail from './send-email';

const minutes = parseFloat(process.env.INTERVAL_MINS) || 1;
const the_interval = minutes * 10 * 1000;

const filtersApartments = {};

async function init() {
  _.forOwn(config.filters, async (filter, filterKey) => {
    const filePath = path.join(__dirname, `../data/${filterKey}.json`);
    let file;
    try {
      file = JSON.parse(fs.readFileSync(filePath, 'utf8')) || {};
    } catch (e) {
      file = {};
    }
    filtersApartments[filterKey] = file;
  })
}

function work() {
  _.forOwn(config.filters, async (filter, filterName) => {
    const items = await njuskalo.getLastThree(filter);
    for (const item of items) {
      //is item already read
      console.log(item);
      if (_.get(filtersApartments, `${filterName}.${item.name}`)) {
        return;
      }
      console.log(`New real estate: ${item.name}`);
      sendMail(`Nova nekretnina za ${filterName} filter!\n${item.href}`)
        .then(console.log)
        .catch(console.log)
      _.set(filtersApartments, `${filterName}.${item.name}`, true);
      const filePath = path.join(__dirname, `../data/${filterName}.json`);
      fs.writeFileSync(filePath, JSON.stringify(filtersApartments[filterName]), 'utf8');
    }
  })
}

init();

setInterval(() => {
  work();
}, the_interval);

