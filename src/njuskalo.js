import request from 'request-promise'
import config from './config';
import cheerio from 'cheerio';
import _ from 'lodash';

function getPostingTime(article) {
  const timeDiv = _.find(_.get(article, 'children'), ['attribs.class', 'entity-pub-date']);
  const time = _.find(_.get(timeDiv, 'children'), ['name', 'time']);
  return _.get(time, 'attribs.datetime');
}

function getName(article) {
  const nameDiv = _.find(_.get(article, 'children'), ['attribs.class', 'entity-title']);
  return _.get(nameDiv, 'children[0].children[0].data');
}

export async function getLastThree(segment) {
  const html = await request({
    method: 'GET',
    uri: config.njuskaloEndpoint + segment,
  });
  const $ = cheerio.load(html);
  const liList = $('.EntityList--Regular .EntityList-item');
  const list = [];
  for (let i = 0; i < 3; i++) {
    const item = _.get(liList, `[${i}]`);
    if (!item) {
      continue;
    }
    const href = `${config.njuskaloEndpoint}${_.get(item, 'attribs.data-href')}`;
    const article = _.find(_.get(item, 'children'), ['name', 'article']);
    const name = getName(article);
    list.push({ name, href });
  }
  return list;
}