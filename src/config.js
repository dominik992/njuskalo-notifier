export default {
  senderEmail: process.env.EMAIL_SENDER || 'njuskalo.notifications@gmail.com',
  senderPassword: process.env.EMAIL_SENDER_PASSWORD || 'njuskalo22',
  receiverEmail: process.env.RECEIVER_EMAIL,
  njuskaloEndpoint: 'http://www.njuskalo.hr',
  filters: {
    TRESNJEVKA_JUG_KUCE:'/prodaja-kuca?locationId=1261',
    TRESNJEVKA_SJEVER_KUCE:'/prodaja-kuca?locationId=1262',
    TRESNJEVKA_JUG_ZEMLJISTA:'/prodaja-zemljista?locationId=1261',
    TRESNJEVKA_SJEVER_ZEMLJISTA:'/prodaja-zemljista?locationId=1262',
    TRESNJEVKA_JUG_NAJAM:'/iznajmljivanje-stanova?locationId=1261',
    TRESNJEVKA_SJEVER_NAJAM:'/iznajmljivanje-stanova?locationId=1262',
  }
};
